require "open3"

class PDFKit

  class NoExecutableError < StandardError
    def initialize(wkhtmltopdf)
      msg  = "No wkhtmltopdf executable found at #{wkhtmltopdf}\n"
      msg << ">> Please install wkhtmltopdf - https://github.com/jdpace/PDFKit/wiki/Installing-WKHTMLTOPDF"
      super(msg)
    end
  end

  class ImproperSourceError < StandardError
    def initialize(msg)
      super("Improper Source: #{msg}")
    end
  end

  attr_accessor :source, :stylesheets, :wkhtmltopdf
  attr_accessor :options

  def initialize(url_file_or_html, options = {})
    @source = Source.new(url_file_or_html)
    @stylesheets = []
    @wkhtmltopdf = PDFKit.configuration.wkhtmltopdf

    @options = PDFKit.configuration.default_options.dup
    @options.merge! find_options_in_meta(url_file_or_html) unless source.url?
    @options.merge!(options)
    @options = normalize_options(@options)

    raise NoExecutableError.new(@wkhtmltopdf) unless File.exists?(@wkhtmltopdf)
  end

  def command(path = nil)
    args = ['timeout --signal=9 41', executable]
    args << '--quiet'
    args += @options.to_a.flatten.compact

    if @source.html?
      args << '-' # Get HTML from stdin
    else
      args << "\"#{@source}\""
    end

    args << (path || '-') # Write to file or stdout
  end

  def executable
    default = @wkhtmltopdf
    return default if default !~ /^\// # its not a path, so nothing we can do
    if File.exist?(default)
      default
    else
      default.split('/').last
    end
  end

  def new_to_pdf(path=nil)
    append_stylesheets

    args = command(path)
    invoke = args.join(' ')

    result, err_str, cmd_status = Open3.capture3(invoke, stdin_data: @source.to_s, binmode: true)
  end

  def to_pdf(path=nil)
    append_stylesheets

    args = command(path)
    invoke = args.join(' ')

    result = IO.popen(invoke, "wb+") do |pdf|
      pdf.puts(@source.to_s) if @source.html?
      pdf.close_write
      pdf.gets(nil) if path.nil?
    end
    result = File.read(path) if path

    raise "command failed: #{invoke}" if result.to_s.strip.empty?
    return result
  end

  def to_file(path)
    self.to_pdf(path)
    File.new(path)
  end

  protected

    def find_options_in_meta(content)
      # Read file if content is a File
      content = content.read if content.is_a?(File)

      found = {}
      content.scan(/<meta [^>]*>/) do |meta|
        if meta.match(/name=["']#{PDFKit.configuration.meta_tag_prefix}/)
          name = meta.scan(/name=["']#{PDFKit.configuration.meta_tag_prefix}([^"']*)/)[0][0]
          found[name.to_sym] = meta.scan(/content=["']([^"']*)/)[0][0]
        end
      end

      found
    end

    def style_tag_for(stylesheet)
      "<style>#{File.read(stylesheet)}</style>".gsub('\\', '\\\\\\\\')
    end

    def append_stylesheets
      raise ImproperSourceError.new('Stylesheets may only be added to an HTML source') if stylesheets.any? && !@source.html?

      stylesheets.each do |stylesheet|
        if @source.to_s.match(/<\/head>/)
          @source = Source.new(@source.to_s.gsub(/(<\/head>)/, style_tag_for(stylesheet)+'\1'))
        else
          @source.to_s.insert(0, style_tag_for(stylesheet))
        end
      end
    end

    def normalize_options(options)
      used_keys = []
      normalized_options = {}

      options.each do |key, value|
        next if !PDFKit::VALID_OPTIONS.include?(key) || !value
        next if used_keys.include?(PDFKit::CONFLICTED_OPTIONS[key])

        if [:margin_top, :margin_bottom, :margin_left, :margin_right].include?(key)
          value = convert_to_inches(value)
        end

        if [:header_left, :header_right, :header_center, :header_html, :footer_left, :footer_right, :footer_center, :footer_html].include?(key)
          next if value.blank?
        end

        normalized_key = "--#{normalize_arg key}"
        normalized_value = normalize_value(value)
        normalized_options[normalized_key] = [:cookie, :custom_header].include?(key) || normalized_value.nil? ? normalized_value : "\"#{normalized_value}\""
        used_keys << key
      end
      normalized_options
    end

    def normalize_arg(arg)
      arg.to_s.downcase.gsub(/[^a-z0-9]/,'-')
    end

    def normalize_value(value)
      v = value.to_s
      return nil if v == "true" || v == "false" || v.empty?
      v.gsub('"', '\"')
    end

    def convert_to_inches(raw_value)
      return raw_value if (unit = raw_value[/\D\D$/]) == 'in'

      value = raw_value.to_f
      new_value = case unit
        when 'mm'
          value * 0.03937
        when 'cm'
          value * 0.3937
        else
          value
        end

      "#{new_value}in"
    end

end

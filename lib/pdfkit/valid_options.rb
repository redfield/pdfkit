class PDFKit
  VALID_OPTIONS = [
    :enable_smart_shrinking, :disable_smart_shrinking,
    :collate, :no_collate,
    :copies,
    :d, :dpi,
    :image_dpi, :image_quality,
    :g, :grayscale,
    :l, :lowquality,
    :B, :margin_bottom,
    :L, :margin_left,
    :R, :margin_right,
    :T, :margin_top,
    :O, :orientation,
    :page_height,
    :s, :page_size,
    :page_width,
    :title,
    :background, :no_background,
    :cookie,
    :debug_javascript, :no_debug_javascript,
    :encoding,
    :images,
    :no_images,
    :n, :disable_javascript, :enable_javascript,
    :javascript_delay,
    :load_error_handling,
    :enable_forms, :disable_forms,
    :minimum_font_size,
    :page_offset,
    :post,
    :p, :proxy,
    :run_script,
    :stop_slow_scripts, :no_stop_slow_scripts,
    :user_style_sheet,
    :window_status,
    :zoom,
    :replace,
    :header_left, :header_right, :header_center, :header_html,
    :header_font_name, :header_font_size, :header_line, :header_spacing,
    :footer_left, :footer_right, :footer_center, :footer_html,
    :footer_font_name, :footer_font_size, :footer_line, :footer_spacing,
    :disable_internal_links, :enable_internal_links,
    :disable_external_links, :enable_external_links
  ]

  CONFLICTED_OPTIONS = {
    collate: :no_collate,
    no_collate: :collate,

    background: :no_background,
    no_background: :background,

    debug_javascript: :no_debug_javascript,
    no_debug_javascript: :debug_javascript,

    disable_javascript: :enable_javascript,
    enable_javascript: :disable_javascript,

    stop_slow_scripts: :no_stop_slow_scripts,
    no_stop_slow_scripts: :stop_slow_scripts,

    disable_internal_links: :enable_internal_links,
    enable_internal_links: :disable_internal_links,

    disable_external_links: :enable_external_links,
    enable_external_links: :disable_external_links,

    enable_forms: :disable_forms,
    disable_forms: :enable_forms,
  }
end

